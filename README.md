# ccxt-base

Project creates a bare minimum ccxt compatible docker image.

CCXT – CryptoCurrency eXchange Trading Library
<https://github.com/ccxt/ccxt/blob/master/README.md>

## Usage in Dockerfile

``` Dockerfile
FROM registry.gitlab.com/famlundgren/public/ccxt-base:latest
RUN apk upgrade --no-cache # Apply latest patches

COPY my-script.py /entrypoint.py
RUN chmod +x /*.py
```
