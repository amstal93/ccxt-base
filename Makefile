BUILD_TAG=0:0/$(subst @,:,$(notdir $(CURDIR)))

all: docker-build

docker-build:
	docker build \
		--pull \
		-t $(BUILD_TAG) \
		.
